#A simple Makefile to create our package

EXECNAME=FriendChess
VERSION=0.8
PACKNAME=$(EXECNAME)-$(VERSION).love

all: $(PACKNAME)

$(PACKNAME): *.lua assets
	zip -9 -q -r $(PACKNAME) *.lua */*.lua assets
	
run: *.lua assets
	love ./

clean:
	rm -f *.love
