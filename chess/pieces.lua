-- Basic Piece Class
TEAM_WHITE = 'White'
TEAM_BLACK = 'Black'

PIECE_GENERIC = 'Generic'
PIECE_PAWN = 'Pawn'
PIECE_ROOT = 'Rook'
PIECE_BISHOP = 'Bishop'
PIECE_KNIGHT = 'Knight'
PIECE_QUEEN = 'Queen'
PIECE_KING = 'King'

Piece = {
    x = 0,
    y = 0,
    tile = nil,
    type = PIECE_GENERIC,
    team = TEAM_WHITE,
    shorthand = 'x',
    moveRange = function(self)
        return {}
    end,
    chatRange = function(self)
        return {}
    end,
    spritesheet = love.graphics.newImage("/assets/sprite/chess-classic.png")
}

Piece.meta = {
    __index = Piece,
    __tostring = function(self)
        return string.format("Piece: %s %s at %s%d", self.team, self.type, toLetter(self.x), self.y)
    end
}

function Piece.new(x, y, team)
    piece = {
        x = x,
        y = y,
        team = team
    }

    piece = setmetatable(piece, Piece.meta)

    return piece
end

function Piece:setTeam(team)
    self.team = team

    x,y,w,h = self.quad:getViewport()
    if team == TEAM_WHITE then
        self.front = 1
        self.quad:setViewport(x, 0, w, h)
    else
        self.front = -1
        self.quad:setViewport(x, 16, w, h)
    end
end

function Piece:getRelativeTile(relX, relY)
    return self.tile:getRelative(relX, relY)
end

function Piece:draw()
    love.graphics.draw(self.spritesheet, self.quad, 0, 0)
end

-- Individual Piece Classes
function Piece__index(class)
    return function(t, key)
        if rawget(t, key) then
            return t[key]
        elseif rawget(class, key) then
            return class[key] 
        elseif rawget(Piece, key) then
            return Piece[key]
        else
            return nil
        end
    end
end

-- Pawn
Pawn = {
    shorthand = 'p',
    type = PIECE_PAWN,
    quadX = 0
}

Pawn.meta = {
    __index = Piece__index(Pawn),
    __tostring = Piece.meta.__tostring
}

function Pawn.new(x, y, team)
    pawn = Piece.new(x, y, team)

    pawn.quad = love.graphics.newQuad(Pawn.quadX, 0, 16, 16, Piece.spritesheet:getWidth(), Piece.spritesheet:getHeight())
    pawn:setTeam(team)

    return setmetatable(pawn, Pawn.meta)
end

function Pawn:moveRange()
    range = {}
    if self:getRelativeTile(0, self.front):empty() then
        table.insert(range,self:getRelativeTile(0, self.front))
    end
    
    return range
end

function Pawn:chatRange()
    range = {}
    
    if not self:getRelativeTile(-1, self.front):empty() and self:getRelativeTile(-1, self.front).piece.team ~= self.team then        
        table.insert(range,self:getRelativeTile(-1, self.front))
    end
    if not self:getRelativeTile(1, self.front):empty() and self:getRelativeTile(1, self.front).piece.team ~= self.team then        
        table.insert(range,self:getRelativeTile(1, self.front))
    end
    
    return range
end

-- Rook
Rook = {
    shorthand = 'r',
    type = PIECE_ROOK,
    quadX = 1*16,
    directions = {
        right = {x = 1, y = 0},
        left = {x = -1, y = 0},
        front = {x = 0, y = 1},
        back = {x = 0,y = -1}
    }
}

Rook.meta = {
    __index = Piece__index(Rook),
    __tostring = Piece.meta.__tostring
}

function Rook.new(x, y, team)
    rook = Piece.new(x, y, team)

    rook.quad = love.graphics.newQuad(Rook.quadX, 0, 16, 16, Piece.spritesheet:getWidth(), Piece.spritesheet:getHeight())
    rook:setTeam(team)

    return setmetatable(rook, Rook.meta)
end

function Rook:moveRange()
    range = {}

    for _,direction in pairs(Rook.directions) do
        nextX = direction.x
        nextY = direction.y
        while self:getRelativeTile(nextX, nextY) ~= ChessTile.Empty do
            if not self:getRelativeTile(nextX, nextY):empty() then
                break
            end
            table.insert(range, self:getRelativeTile(nextX, nextY))
            nextX = nextX + direction.x
            nextY = nextY + direction.y
        end
    end
    
    return range
end

function Rook:chatRange()
    range = {}

    for _,direction in pairs(Rook.directions) do
        nextX = direction.x
        nextY = direction.y
        while self:getRelativeTile(nextX, nextY) ~= ChessTile.Empty do
            if not self:getRelativeTile(nextX, nextY):empty() then
                if self:getRelativeTile(nextX, nextY).piece.team ~= self.team then
                    table.insert(range, self:getRelativeTile(nextX, nextY))
                end
                break
            end
            nextX = nextX + direction.x
            nextY = nextY + direction.y
        end
    end

    return range
end

-- Bishop
Bishop = {
    shorthand = 'b',
    type = PIECE_BISHOP,
    quadX = 2*16,
    directions = {
        frontright = {x = 1, y = 1},
        frontleft = {x = -1, y = 1},
        backright = {x = 1, y = -1},
        backleft = {x = -1,y = -1}
    }
}

Bishop.meta = {
    __index = Piece__index(Bishop),
    __tostring = Piece.meta.__tostring
}

function Bishop.new(x, y, team)
    bishop = Piece.new(x, y, team)

    bishop.quad = love.graphics.newQuad(Bishop.quadX, 0, 16, 16, Piece.spritesheet:getWidth(), Piece.spritesheet:getHeight())
    bishop:setTeam(team)

    return setmetatable(bishop, Bishop.meta)
end

function Bishop:moveRange()
    range = {}

    for _,direction in pairs(Bishop.directions) do
        nextX = direction.x
        nextY = direction.y
        while self:getRelativeTile(nextX, nextY) ~= ChessTile.Empty do
            if not self:getRelativeTile(nextX, nextY):empty() then
                break
            end
            table.insert(range, self:getRelativeTile(nextX, nextY))
            nextX = nextX + direction.x
            nextY = nextY + direction.y
        end
    end
    
    return range
end

function Bishop:chatRange()
    range = {}

    for _,direction in pairs(Bishop.directions) do
        nextX = direction.x
        nextY = direction.y
        while self:getRelativeTile(nextX, nextY) ~= ChessTile.Empty do
            if not self:getRelativeTile(nextX, nextY):empty() then
                if self:getRelativeTile(nextX, nextY).piece.team ~= self.team then
                    table.insert(range, self:getRelativeTile(nextX, nextY))
                end
                break
            end
            nextX = nextX + direction.x
            nextY = nextY + direction.y
        end
    end

    return range
end

-- Knight
Knight = {
    shorthand = 'n',
    type = PIECE_KNIGHT,
    quadX = 3*16,
    directions = {
        {x = 1, y = 1},
        {x = -1, y = 1},
        {x = 1, y = -1},
        {x = -1, y = -1}
    },
    lengths = {
        {x = 2, y = 1},
        {x = 1, y = 2}
    }
}

Knight.meta = {
    __index = Piece__index(Knight),
    __tostring = Piece.meta.__tostring
}

function Knight.new(x, y, team)
    knight = Piece.new(x, y, team)

    knight.quad = love.graphics.newQuad(Knight.quadX, 0, 16, 16, Piece.spritesheet:getWidth(), Piece.spritesheet:getHeight())
    knight:setTeam(team)

    return setmetatable(knight, Knight.meta)
end

function Knight:moveRange()
    range = {}

    for _,length in ipairs(Knight.lengths) do
        for _,direction in ipairs(Knight.directions) do
            nextX = direction.x*length.x
            nextY = direction.y*length.y
            if self:getRelativeTile(nextX, nextY):empty() then
                table.insert(range, self:getRelativeTile(nextX, nextY))
            end
        end
    end
    
    return range
end

function Knight:chatRange()
    range = {}

    for _,length in ipairs(Knight.lengths) do
        for _,direction in ipairs(Knight.directions) do
            nextX = direction.x*length.x
            nextY = direction.y*length.y
            if not self:getRelativeTile(nextX, nextY):empty() then
                if self:getRelativeTile(nextX,nextY).piece.team ~= self.team then 
                    table.insert(range, self:getRelativeTile(nextX, nextY))
                end
            end
        end
    end

    return range
end

-- Queen
Queen = {
    shorthand = 'q',
    type = PIECE_QUEEN,
    quadX = 4*16,
    directions = {
        right = {x = 1, y = 0},
        left = {x = -1, y = 0},
        front = {x = 0, y = 1},
        back = {x = 0,y = -1},
        frontright = {x = 1, y = 1},
        frontleft = {x = -1, y = 1},
        backright = {x = 1, y = -1},
        backleft = {x = -1,y = -1}
    }
}

Queen.meta = {
    __index = Piece__index(Queen),
    __tostring = Piece.meta.__tostring
}

function Queen.new(x, y, team)
    queen = Piece.new(x, y, team)

    queen.quad = love.graphics.newQuad(Queen.quadX, 0, 16, 16, Piece.spritesheet:getWidth(), Piece.spritesheet:getHeight())
    queen:setTeam(team)

    return setmetatable(queen, Queen.meta)
end

function Queen:moveRange()
    range = {}

    for _,direction in pairs(Queen.directions) do
        nextX = direction.x
        nextY = direction.y
        while self:getRelativeTile(nextX, nextY) ~= ChessTile.Empty do
            if not self:getRelativeTile(nextX, nextY):empty() then
                break
            end
            table.insert(range, self:getRelativeTile(nextX, nextY))
            nextX = nextX + direction.x
            nextY = nextY + direction.y
        end
    end
    
    return range
end

function Queen:chatRange()
    range = {}

    for _,direction in pairs(Queen.directions) do
        nextX = direction.x
        nextY = direction.y
        while self:getRelativeTile(nextX, nextY) ~= ChessTile.Empty do
            if not self:getRelativeTile(nextX, nextY):empty() then
                if self:getRelativeTile(nextX, nextY).piece.team ~= self.team then
                    table.insert(range, self:getRelativeTile(nextX, nextY))
                end
                break
            end
            nextX = nextX + direction.x
            nextY = nextY + direction.y
        end
    end

    return range
end

-- King
King = {
    shorthand = 'k',
    type = PIECE_KING,
    quadX = 5*16,
    directions = {
        right = {x = 1, y = 0},
        left = {x = -1, y = 0},
        front = {x = 0, y = 1},
        back = {x = 0,y = -1},
        frontright = {x = 1, y = 1},
        frontleft = {x = -1, y = 1},
        backright = {x = 1, y = -1},
        backleft = {x = -1,y = -1}
    }
}

King.meta = {
    __index = Piece__index(King),
    __tostring = Piece.meta.__tostring
}

function King.new(x, y, team)
    king = Piece.new(x, y, team)

    king.quad = love.graphics.newQuad(King.quadX, 0, 16, 16, Piece.spritesheet:getWidth(), Piece.spritesheet:getHeight())
    king:setTeam(team)

    return setmetatable(king, King.meta)
end

function King:moveRange()
    range = {}

    for _,direction in pairs(King.directions) do
        if self:getRelativeTile(direction.x, direction.y):empty() then
            table.insert(range, self:getRelativeTile(direction.x, direction.y))
        end
    end
    
    return range
end

function King:chatRange()
    range = {}

    for _,direction in pairs(King.directions) do
        if not self:getRelativeTile(direction.x, direction.y):empty() then
            if self:getRelativeTile(direction.x, direction.y).piece.team ~= self.team then
                table.insert(range, self:getRelativeTile(direction.x, direction.y))
            end
        end
    end

    return range
end
