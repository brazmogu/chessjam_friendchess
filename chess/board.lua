-- The Chess Board
ChessBoard = {
    width = 8,
    height = 8,
    tiles = {}
}

ChessBoard.meta = {
    __index = ChessBoard,
    __tostring = function(self)
        repr = ""
        for y = 1,board.height do
            repr = repr .. y
            for x = 1,board.width do
                repr = repr .. " "
                tile = self:getTile(x,y)
                if tile:empty() then
                    repr = repr .. "#"
                else
                    if tile.piece.team == TEAM_WHITE then
                        repr = repr .. tile.piece.shorthand:upper()
                    else
                        repr = repr .. tile.piece.shorthand:lower()
                    end
                end
            end
            repr = repr .. "\n"
        end

        repr = repr .. " "
        for x = 1,board.width do
            repr = repr .. " " .. toLetter(x)
        end

        return repr
    end
}

function ChessBoard.new(w, h)
    board = {
        width = w,
        height = h,
        tiles = {},
        turn = TEAM_WHITE
    }

    board = setmetatable(board, ChessBoard.meta)

    for x = 1,board.width do
        board.tiles[toLetter(x)] = {} -- Columns as Letters
        for y = 1,board.height do
            board.tiles[toLetter(x)][y] = ChessTile.new(x, y, board)
        end
    end

    Game.victory = nil

    return board
end

function ChessBoard:getTile(x, y)
    if x < 1 or x > self.width or y < 1 or y > self.width then
        return ChessTile.Empty
    else
        return self.tiles[toLetter(x)][y]
    end
end

function ChessBoard:putPiece(x, y, class, team)
    tile = self:getTile(x,y)
    if tile ~= ChessTile.Empty then
        tile:putPiece(class.new(x,y,team))
    end
end

function ChessBoard:clearSelection()
    Game.sfx.unpick:play()
    
    if self.selected ~= nil then
        self.selected:deselect()
    end

    self.selected = nil
    self.moveRange = {}
    self.chatRange = {}
end

function ChessBoard:selectTile(x, y)
    candidate = self:getTile(x, y)
    
    if candidate == self.selected or candidate == ChessTile.Empty then
        self:clearSelection()
    else
        if self.selected ~= nil then
            -- check if it is a valid move
            
            for _,tile in ipairs(self.moveRange) do
                if tile == candidate then
                    print("valid move")
                    Game.sfx.move:play()
                    self:move(self.selected, candidate)
                    self.selected = nil
                    return
                end
            end
            for _,tile in ipairs(self.chatRange) do
                if tile == candidate then
                    print("valid conversion")
                    Game.sfx.chat:play()
                    self:chat(self.selected, candidate)
                    self.selected = nil
                    return
                end
            end
            
            self.selected:deselect()
        end

        self.selected = candidate
        self.moveRange = {}
        self.chatRange = {}
        if self.selected.piece then
            if self.selected.piece.team == self.turn then
                self.moveRange = self.selected.piece:moveRange()
                self.chatRange = self.selected.piece:chatRange()
            end
        end

        candidate:select()
    end
end

function ChessBoard:passTurn()
    if self.turn == TEAM_WHITE then
        self.turn = TEAM_BLACK
    else
        self.turn = TEAM_WHITE
    end
end

function ChessBoard:checkVictory()
    blackKing = nil
    whiteKing = nil
    for _,row in pairs(self.tiles) do
        for _,tile in ipairs(row) do
            if not tile:empty() then
                if tile.piece.type == PIECE_KING then
                    if tile.piece.team == TEAM_BLACK then
                        blackKing = tile.piece
                    end
                    if tile.piece.team == TEAM_WHITE then
                        whiteKing = tile.piece
                    end
                end
            end
        end
    end

    if whiteKing and not blackKing then
        ScreenStack:push(EndScreen)
        return TEAM_WHITE
    end

    if blackKing and not whiteKing then
        ScreenStack:push(EndScreen)
        return TEAM_BLACK
    end

    return nil
end

function ChessBoard:move(from, to)
    to:putPiece(from:takePiece())
    self:passTurn()
end

function ChessBoard:chat(from, to)
    to.piece:setTeam(from.piece.team)
    Game.victory = self:checkVictory()
    
    if Game.victory == nil then
        self:passTurn()
    end
end

function ChessBoard:draw(x, y)
    love.graphics.push()
    love.graphics.translate(x,y)

    tileTint = {}

    if self.selected then
        tileTint[self.selected] = CHESS_TILE_COLOR_SELECTED
        for _,tile in ipairs(self.moveRange) do
            tileTint[tile] = CHESS_TILE_COLOR_MOVE
        end
        for _,tile in ipairs(self.chatRange) do
            tileTint[tile] = CHESS_TILE_COLOR_CHAT
        end
    end

    x = 1
    y = 1
    even = true
    evenLine = true 
    for i = 1,board.width*board.height do
        tile = self:getTile(x,y)
        tile:draw(even, tileTint[tile])
        x = x+1
        even = not even
        if x > self.width then
            x = 1
            y = y+1
            evenLine = not evenLine
            even = evenLine
        end
    end

    if not Game.victory then
        love.graphics.translate(0,-32)
        prompt = self.turn:upper() .. " TURN"
        love.graphics.setColor(255, 255, 255)
        love.graphics.printf(prompt, 0, 0, 8*CHESS_TILE_SIZE, "center")
    end

    love.graphics.pop()
end
