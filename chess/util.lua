-- Utilities

function toLetter(n)
    if n <= 0 then return '#' end
    return string.char(string.byte('A') + n - 1)
end
