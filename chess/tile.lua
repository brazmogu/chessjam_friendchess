--Chess Tile
ChessTile = {
    x = 0,
    y = 0,
    piece = nil,
    board = nil
}

ChessTile.meta = {
    __index = ChessTile,
    __tostring = function(self)
        if self.piece == nil then
            return string.format("Empty tile at %s%d", toLetter(self.x), self.y)
        else
            return string.format("Tile at %s%d containing a %s %s", toLetter(self.x), self.y, self.piece.team, self.piece.type)
        end
    end
}

function ChessTile.new(x, y, board)
    tile = {
        x = x,
        y = y,
        board = board,
        selected = false
    }

    tile = setmetatable(tile, ChessTile.meta)

    return tile
end

function ChessTile:getRelative(relX, relY)
    return self.board:getTile(self.x + relX, self.y + relY)
end

function ChessTile:takePiece()
    _piece = self.piece
    if _piece == nil then
        error(string.format("Attempting to take piece from empty tile at %s%d", self.x, self.y))
    end

    _piece.tile = nil
    self.piece = nil
    return _piece
end

function ChessTile:putPiece(piece)
    piece.x = self.x
    piece.y = self.y
    piece.tile = self
    self.piece = piece
end

function ChessTile:empty()
    return self.piece == nil
end

function ChessTile:select()
    Game.sfx.pick:play()
    self.selected = true
end

function ChessTile:deselect()
    self.selected = false
end

function ChessTile:draw(even, tint)
    love.graphics.push()
    
    if even then
        love.graphics.setColor(unpack(CHESS_TILE_COLOR_EVEN))
    else
        love.graphics.setColor(unpack(CHESS_TILE_COLOR_ODD))
    end
    love.graphics.translate((self.x-1) * CHESS_TILE_SIZE, (self.y-1) * CHESS_TILE_SIZE)
    love.graphics.rectangle("fill", 0, 0, CHESS_TILE_SIZE, CHESS_TILE_SIZE)
    
    if tint then
        love.graphics.push()
        love.graphics.setColor(unpack(tint))
        love.graphics.rectangle("fill", 1, 1, CHESS_TILE_SIZE-2, CHESS_TILE_SIZE-2)
        love.graphics.pop()
    end

    if not self:empty() then
        love.graphics.setColor(255, 255, 255)
        self.piece:draw()
    end

    love.graphics.setColor(255, 255, 255)
    love.graphics.pop()
end

-- Empty Tile: represents an empty tile, to be used instead of nil
ChessTile.Empty = ChessTile.new(0, 0)

function ChessTile.Empty:getRileRelative()
    return self
end