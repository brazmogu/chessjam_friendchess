-- Screen control
Screen = {
    overlap = false
}

function Screen.new(overlap)
    screen = {
        initialized = false,
        overlap = overlap
    }

    screen = setmetatable(screen, {__index = Screen})
    return screen
end

-- Run when the screen is first added to the stack
function Screen:init()
end

-- Run when the screen becomes the top to the stack
function Screen:show()
end

-- Run when the screen is taken off the stack
function Screen:close()
end

-- Run on the top screen at each update
function Screen:update(dt)
end

-- Handle input on the top screen
function Screen:input(event, data)
end

-- Run on all the screens that can overlap
function Screen:draw()
end

-- Screen Stack
ScreenStack = {
    screens = {},
    top = 0,
    canvas = nil
}

-- Initialize Screen Stack graphic mode
ScreenStack.canvas = love.graphics.newCanvas(love.graphics.getWidth()/SCREEN_SCALE, love.graphics.getHeight()/SCREEN_SCALE)
ScreenStack.canvas:setFilter("nearest", "nearest")

function ScreenStack:push(screen)
    if self:peek() ~= nil then
        self:peek():close()
    end

    table.insert(self.screens, self.top+1, screen)
    self.top = self.top + 1

    if not screen.initialized then
        screen.initalized = true
        screen:init()
    end
    screen:show()
    print (#ScreenStack.screens)
end

function ScreenStack:peek()
    if self.top == 0 then
        return nil
    end

    return self.screens[self.top]
end

function ScreenStack:pop()
    if self.top == 0 then
        return nil
    end

    topScreen = self.screens[self.top]
    table.remove(self.screens, self.top)
    self.top = self.top-1
    topScreen:close()

    if self:peek() ~= nil then
        self:peek():show()
    else
        love.event.quit()
    end

    print (#ScreenStack.screens)
    
    return topScreen
end

function ScreenStack:update(dt)
    if self:peek() ~= nil then
        self:peek():update(dt)
    end
end

function ScreenStack:input(event, data)
    if self:peek() ~= nil then
        self:peek():input(event, data)
    end
end

function ScreenStack:draw()
    love.graphics.setCanvas(self.canvas)
    love.graphics.clear()

    if self.top > 0 then
        bottom = self.top
        while self.top > 1 and self.screens[bottom].overlap do
            bottom = bottom - 1
        end

        for screen = bottom,self.top do
            self.screens[screen]:draw()
        end
    end

    love.graphics.setCanvas()
    love.graphics.draw(self.canvas, 0, 0, 0, SCREEN_SCALE, SCREEN_SCALE)
end