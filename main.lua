-- Constants
SCREEN_SCALE = 2
CHESS_TILE_SIZE = 16

CHESS_TILE_COLOR_EVEN = {0xAA, 0x80, 0x80}
CHESS_TILE_COLOR_ODD = {0x20, 0x80, 0xAA}
CHESS_TILE_COLOR_SELECTED = {0x80, 0xC0, 0xFF}
CHESS_TILE_COLOR_MOVE = {0x80, 0xFF, 0xC0}
CHESS_TILE_COLOR_CHAT = {0xFF, 0x80, 0xC0}

require("chess/util")

require("chess/board")
require("chess/tile")
require("chess/pieces")

require("screen")
require("screen/game")
require("screen/title")
require("screen/pause")
require("screen/endgame")

Game = {}

-- Game Flow
function love.load()
    love.graphics.setDefaultFilter("nearest", "nearest")
    love.graphics.setFont(love.graphics.newFont("/assets/font/SomepxNew.ttf", 16))

    Game.sfx = {
        pick = love.audio.newSource("/assets/sfx/select.wav"),
        unpick = love.audio.newSource("/assets/sfx/deselect.wav"),
        move = love.audio.newSource("/assets/sfx/move.wav"),
        chat = love.audio.newSource("/assets/sfx/chat.wav")
    }
    Game.sfx.move:setVolume(0.25)
    Game.sfx.chat:setVolume(0.4)

    ScreenStack:push(TitleScreen)
end

function love.init()
end

function love.update(dt)
end

function love.quit()
end

-- Draw
function love.draw()
    ScreenStack:draw()
end

-- Input Handling
function love.mousepressed(x, y, button, istouch)
    ScreenStack:input("mouse", {x = x, y = y, button = button})
end

function love.keypressed(key, scancode, isrepeat)
    if key == "p" then
        print("Shot!")
        sshot = love.graphics.newScreenshot():encode('png', "sshot_"..os.time()..".png")
        print(sshot:getFilename())
    end
    
    ScreenStack:input("key", {key = key, isrepeat = isrepeat})
end

