-- Title Screen
TitleScreen = Screen.new()
TitleScreen.bg = love.graphics.newImage("/assets/sprite/chess-title.png")

-- Run when the screen is first added to the stack
function TitleScreen:init()
end

-- Run when the screen becomes the top to the stack
function TitleScreen:show()
end

-- Run when the screen is taken off the stack
function TitleScreen:close()
end

-- Run on the top screen at each update
function TitleScreen:update(dt)
end

-- Handle input on the top screen
function TitleScreen:input(event, data)
    if event == "mouse" then
        self.menu:click(data.x/SCREEN_SCALE, data.y/SCREEN_SCALE)
    end
end

-- Run on all the screens that can overlap
function TitleScreen:draw()
    love.graphics.draw(self.bg, 0, 0)
    self.menu:draw()
end

-- Main Menu
TitleScreen.menu = {
    x = love.graphics.getWidth()/SCREEN_SCALE/2 - 128/2,
    y = love.graphics.getHeight()/SCREEN_SCALE - 64 - 16,
    width = 128,
    height = 64,
    buttons = {
        height = 32, 
        hPad = 1.0/8,
        vPad = 1.0/4,
        {
            label = 'Start',
            click = function()
                ScreenStack:push(GameScreen)
            end
        },
        {
            label = 'Quit',
            click = function()
                ScreenStack:pop()
            end
        }
    }
}

function TitleScreen.menu:click(x, y)
    relX = x - self.x
    relY = y - self.y
    print(relX, relY)
    if relX >= 0 and relY >= 0 then
        if relX <= self.width and relY <= self.height then
            for i,button in ipairs(self.buttons) do
                relButtonY = relY - (i-1)*self.buttons.height
                print(relX, relButtonY)
                if relX >= self.buttons.hPad * self.width and relX <= (1-self.buttons.hPad) * self.width
                    and relButtonY >= self.buttons.vPad * self.buttons.height and relButtonY <= (1-self.buttons.vPad) * self.buttons.height then
                        print(button.label .. " Click!")
                        button.click()
                end
            end
        end
    end
end

function TitleScreen.menu:draw()
    love.graphics.push()
    love.graphics.translate(self.x, self.y)

    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle("fill", 0, 0, self.width, self.height)
    love.graphics.setColor(255, 255, 255)
    love.graphics.setLineWidth(0.5)
    love.graphics.rectangle("line", 0.5, 0.5, self.width-1, self.height-1)
    love.graphics.rectangle("line", 2.5, 2.5, self.width-5, self.height-5)

    -- Draw Buttons
    for i,button in ipairs(self.buttons) do
        love.graphics.push()
            love.graphics.translate(0, (i-1)*32)
            love.graphics.translate(self.width*self.buttons.hPad, self.buttons.height*self.buttons.vPad)
            love.graphics.setColor(0, 0, 0)
            love.graphics.rectangle("fill", 0, 0, self.width*(1-2*self.buttons.hPad), self.buttons.height*(1-2*self.buttons.vPad))
            love.graphics.setColor(255, 255, 255)
            love.graphics.setLineWidth(0.5)
            love.graphics.rectangle("line", 0.5, 0.5, self.width*(1-2*self.buttons.hPad) - 1, self.buttons.height*(1-2*self.buttons.vPad) - 1)
            love.graphics.printf(button.label, 0, 0, self.width*(1-2*self.buttons.hPad), "center")        
        love.graphics.pop()
    end

    love.graphics.pop()
end