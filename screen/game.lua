-- Game Screen
GameScreen = Screen.new()

function GameScreen:init()
    self.board = ChessBoard.new(8,8)
    self.boardX = love.graphics.getWidth()/SCREEN_SCALE/2 - 4*CHESS_TILE_SIZE
    self.boardY = love.graphics.getHeight()/SCREEN_SCALE/2 - 4*CHESS_TILE_SIZE

    -- Add Pawns
    for i = 1,8 do
        self.board:putPiece(i, 2, Pawn, 'White')
        self.board:putPiece(i, 7, Pawn, 'Black')
    end

    -- Add Rooks
    self.board:putPiece(1, 1, Rook, 'White')
    self.board:putPiece(8, 1, Rook, 'White')
    self.board:putPiece(1, 8, Rook, 'Black')
    self.board:putPiece(8, 8, Rook, 'Black')

    -- Add Knights
    self.board:putPiece(2, 1, Knight, 'White')
    self.board:putPiece(7, 1, Knight, 'White')
    self.board:putPiece(2, 8, Knight, 'Black')
    self.board:putPiece(7, 8, Knight, 'Black')

    -- Add Bishops
    self.board:putPiece(3, 1, Bishop, 'White')
    self.board:putPiece(6, 1, Bishop, 'White')
    self.board:putPiece(3, 8, Bishop, 'Black')
    self.board:putPiece(6, 8, Bishop, 'Black')

    -- Add Queens and Kings
    self.board:putPiece(4, 1, Queen, 'White')
    self.board:putPiece(5, 1, King, 'White')
    self.board:putPiece(4, 8, Queen, 'Black')
    self.board:putPiece(5, 8, King, 'black')

    print (self.board)
end

function GameScreen:open()
end

function GameScreen:close()
end

function GameScreen:update(dt)
end

function GameScreen:input(event, data)
    if event == "mouse" then
        tileX = math.floor((data.x - self.boardX*2)/SCREEN_SCALE/CHESS_TILE_SIZE + 1)
        tileY = math.floor((data.y - self.boardY*2)/SCREEN_SCALE/CHESS_TILE_SIZE + 1)

        if data.button == 1 then
            self.board:selectTile(tileX, tileY)
        end
    end

    if event == "key" then
        if data.key == "escape" then
            ScreenStack:push(PauseScreen)
        end
    end
end

function GameScreen:draw()
    self.board:draw(self.boardX, self.boardY)
end