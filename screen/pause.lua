-- Pause menu
PauseScreen = Screen.new(true)
PauseScreen.width = 144
PauseScreen.height = 96

-- Run when the screen is first added to the stack
function PauseScreen:init()
end

-- Run when the screen becomes the top to the stack
function PauseScreen:show()
end

-- Run when the screen is taken off the stack
function PauseScreen:close()
end

-- Run on the top screen at each update
function PauseScreen:update(dt)
end

-- Handle input on the top screen
function PauseScreen:input(event, data)
    if event == "mouse" then
        self.menu:click(data.x/SCREEN_SCALE, data.y/SCREEN_SCALE)
    end

    if event == "key" then
        if data.key == "escape" then
            ScreenStack:pop()
        end
    end
end

-- Run on all the screens that can overlap
function PauseScreen:draw()
    love.graphics.push()
    
    self.menu:draw()

    love.graphics.pop()
end

-- Pause Menu
PauseScreen.menu = {
    x = (love.graphics.getWidth()/SCREEN_SCALE - PauseScreen.width)/2,
    y = (love.graphics.getHeight()/SCREEN_SCALE - PauseScreen.height)/2,
    width = PauseScreen.width,
    height = PauseScreen.height,
    buttons = {
        height = 48,
        hPad = 1.0/8,
        vPad = 1.0/4,
        {
            label = 'Continue',
            click = function()
                ScreenStack:pop()
            end
        },
        {
            label = 'Quit',
            click = function()
                ScreenStack:pop()
                ScreenStack:pop()
            end
        }
    }
}

function PauseScreen.menu:click(x, y)
    relX = x - self.x
    relY = y - self.y
    print(relX, relY)
    if relX >= 0 and relY >= 0 then
        if relX <= self.width and relY <= self.height then
            for i,button in ipairs(self.buttons) do
                relButtonY = relY - (i-1)*self.buttons.height
                print(relX, relButtonY)
                if relX >= self.buttons.hPad * self.width and relX <= (1-self.buttons.hPad) * self.width
                    and relButtonY >= self.buttons.vPad * self.buttons.height and relButtonY <= (1-self.buttons.vPad) * self.buttons.height then
                        print(button.label .. " Click!")
                        button.click()
                end
            end
        end
    end
end

function PauseScreen.menu:draw()
    love.graphics.translate(self.x, self.y)
    
    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle("fill", 0, 0, self.width, self.height)
    love.graphics.setColor(255, 255, 255)
    love.graphics.rectangle("line", 0.5, 0.5, self.width-1, self.height-1)

    for i,button in ipairs(self.buttons) do
        love.graphics.push()
            love.graphics.translate(0, (i-1)*self.buttons.height)
            love.graphics.translate(self.width*self.buttons.hPad, self.buttons.height*self.buttons.vPad)
            love.graphics.setColor(0, 0, 0)
            love.graphics.rectangle("fill", 0, 0, self.width*(1-2*self.buttons.hPad), self.buttons.height*(1-2*self.buttons.vPad))
            love.graphics.setColor(255, 255, 255)
            love.graphics.setLineWidth(0.5)
            love.graphics.rectangle("line", 0.5, 0.5, self.width*(1-2*self.buttons.hPad) - 1, self.buttons.height*(1-2*self.buttons.vPad) - 1)
            love.graphics.printf(button.label, 0, 0, self.width*(1-2*self.buttons.hPad), "center")        
        love.graphics.pop()
    end
end